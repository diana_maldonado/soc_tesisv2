#include "spi.h"
#include "timer.h"
#include "ili9341.h"
#include "read_buffer.h"
#include "cam_config.h"
#include "preprocessing_HW.h"
#include "preprocessing_SW.h"
#include "defaults.h"
#include "RTC.h"

#include <stdio.h>
#include <stdlib.h>
#include <irq.h>
#include <uart.h>
#include <i2c.h>
#include <console.h>
#include <generated/csr.h>

int32_t sw_time[10];
int j = 0;

int main(void){
   irq_setmask((1 << PREPROCESSOR_INTERRUPT) + (1 << UART_INTERRUPT));
	irq_setie(1);
   preprocessor_ev_enable_write(3);
   uart_init();
   set_spi_prescaler(100);
   wait_ms(10);
   spi_init();
   ili9341_init();
   enable_HW_preprocessing(1);
   enable_HW_bypass();
   set_switch_state_hw_sw(1);
   set_switch_state_prep_filter(0);
   ili9341_setRotation(1);
   ili9341_clear(ILI9341_BLACK);
   set_spi_prescaler(6);
   wait_ms(10);
   cam_init_reg();

   while(1){
      if(get_enable_mode_state() == 0){
         while(!preprocessor_prep_allowed_reg_CSR_read());
         if(get_switch_state_prep_filter()==0){
            milisec_actual_SW();
            SW_bypass();
            milisec_prepro_SW();
         } else if(get_switch_state_prep_filter()==1){
            milisec_actual_SW();
            SW_grayscale();
            milisec_prepro_SW();
         } else if(get_switch_state_prep_filter()==2){
            milisec_actual_SW();
            SW_threshold(31);
            milisec_prepro_SW();
         } else if(get_switch_state_prep_filter()==3){
            milisec_actual_SW();
            SW_invert();
            milisec_prepro_SW();
         }
         sw_time[j] = get_milisec_prepro_SW();
         j=j+1;
         preprocessor_prep_completed_SW_reg_write(1);
         preprocessor_prep_completed_SW_reg_write(0);
         while(preprocessor_prep_allowed_reg_CSR_read());
         enable_HW_preprocessing(get_switch_state_hw_sw());
      }
      set_switch_state_hw_sw(switches_in_read() & 0x01);
      set_switch_state_prep_filter(switches_in_read()>>1);
      rq_read();
      ili9341_writeFrame(&read_px, &read_px_init);
      end_reading();
      if(button_in_read() == 1){
         printf( "\n");
         printf( "HW time: ");
         for (int i= 0; i<10; i++){ 
            printf( "%d, ", hw_time[i]);
         }
         printf( "\n");
         printf( "SW time: ");
         for (int j= 0; j<10; j++){ 
            printf( "%d, ", sw_time[j]);
         }
      }
   }
   return 0;
}