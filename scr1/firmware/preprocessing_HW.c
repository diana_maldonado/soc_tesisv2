#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
//#include <string.h>
#include <stdbool.h>
#include <generated/csr.h>

#include "preprocessing_HW.h"


void enable_HW_preprocessing(bool value){
    preprocessor_prep_enable_mode_reg_write(!value);
}

uint8_t get_enable_mode_state(void){
    return !preprocessor_prep_enable_mode_reg_read();
}

void enable_HW_bypass(void){
    preprocessor_prep_filter_reg_write(0);
}

void enable_HW_grayscale(void){
    preprocessor_prep_filter_reg_write(1);
}

void enable_HW_threshold(void){
    preprocessor_prep_filter_reg_write(2);
}

void enable_HW_invert(void){
    preprocessor_prep_filter_reg_write(3);
}