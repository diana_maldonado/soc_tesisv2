#include "read_buffer.h"
#include "timer.h"

#include <generated/csr.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
//#include <string.h>

void rq_read(void){
    prep_buffer_rq_read_reg_write(0x01);
    while(prep_buffer_ack_read_reg_read()==0);
    prep_buffer_rq_read_reg_write(0x00);
    prep_buffer_reading_reg_write(0x01);
}

void end_reading(void){
    prep_buffer_reading_reg_write(0x00);
    while(prep_buffer_ack_read_reg_read()==1);
}

uint16_t read_px(uint32_t address){
    uint16_t data;
    // wait_us(1);
    prep_buffer_read_clk_reg_write(0x01);
    // wait_us(1);
    prep_buffer_read_clk_reg_write(0x00);
    data = prep_buffer_output_px_data_reg_read();
    prep_buffer_read_addr_reg_write(address);    
    return data;
}

void read_px_init(void){
    // wait_us(1);
    prep_buffer_read_clk_reg_write(0x01);
    // wait_us(1);
    prep_buffer_read_clk_reg_write(0x00);
    prep_buffer_read_addr_reg_write(0x00);
}