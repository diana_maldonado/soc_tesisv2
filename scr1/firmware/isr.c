#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <generated/csr.h>
#include <generated/soc.h>
#include <irq.h>
#include <console.h>
#include <uart.h>

#include "RTC.h"
#include "preprocessing_HW.h"
#include "defaults.h"

int i = 0;

void isr(void);
extern void wait_ms(unsigned int time);

void isr(void){
	unsigned int irqs;

	irqs = irq_pending() & irq_getmask();

	if(irqs & (1 << UART_INTERRUPT)){
		uart_isr();
	}else if(irqs & (1 << PREPROCESSOR_INTERRUPT)){
		if(preprocessor_ev_pending_start_prep_read()){
			milisec_actual_HW();
			preprocessor_ev_pending_start_prep_write(1);
		} else if(preprocessor_ev_pending_finish_prep_read()){
			milisec_prepro_HW();
			enable_HW_preprocessing(get_switch_state_hw_sw());
			hw_time[i] = get_milisec_prepro_HW();
			i=i+1;
      		switch(get_switch_state_prep_filter()){
				case 0:
      		   		enable_HW_bypass();
					break;
      			case 1:
      		   		enable_HW_grayscale();
					break;
				case 2:
      		   		enable_HW_threshold();
					break;
				case 3:
					enable_HW_invert();
					break;
				default:
					enable_HW_bypass();
					break;
      		}
			preprocessor_ev_pending_finish_prep_write(1);
		}
	}

}


