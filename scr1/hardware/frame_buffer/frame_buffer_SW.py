from turtle import width
from migen import *
from litex.soc.interconnect.csr import *

import math

# Main module
class frame_buffer_SW(Module,AutoCSR):
    def __init__(self, DEBUG_READ_MODE=0):
    
    ##Parameters
        depth = 76800
        width = 15

        self.zero_signal                   = Signal()  

    ## Inputs        
        self.BUFFER_CLK                    = Signal()
        self.RESET                         = Signal()

        self.WRITE_CLK                     = Signal()                                 #write addressing signals 
        self.INPUT_PX_DATA                 = Signal(width)                               #write addressing signals 
        self.WRITE_ADDR                    = Signal(math.ceil(math.log(depth,2)))     #write addressing signals
        self.ENABLE_MEM                    = Signal()
        self.RQ_WRITE                      = Signal()                                 #write control signals
        self.WRITING                       = Signal()	                                #write control signals	                                #write control signals
        
    ## Outputs
        self.OUTPUT_PX_DATA                = Signal(width)    #read addressing signals 
        self.ACK_READ                      = Signal()      #read control signals
        self.ACK_WRITE                     = Signal()      #write control signals

    ## Internal Registers        
        self.read_clk_reg                  = CSRStorage()
        self.output_px_data_reg            = CSRStatus(15)
        self.read_addr_reg                 = CSRStorage(math.ceil(math.log(depth,2)))
        self.rq_read_reg                   = CSRStorage()
        self.reading_reg                   = CSRStorage()      
        self.ack_read_reg                  = CSRStatus()

    ## Instances
        self.specials +=Instance("frame_buffer",
            p_depth                      = depth,
            p_DEBUG_READ_MODE            = DEBUG_READ_MODE,
            i_buffer_clk                 = self.BUFFER_CLK,    
            i_reset                      = self.RESET,
            i_write_clk                  = self.WRITE_CLK, 
            i_input_px_data              = self.INPUT_PX_DATA,
            i_write_addr                 = self.WRITE_ADDR,
            i_enable_mem                 = self.ENABLE_MEM,
            i_rq_write                   = self.RQ_WRITE,
            i_writing                    = self.WRITING,
            o_ack_write                  = self.ACK_WRITE,
            i_read_clk                   = self.read_clk_reg.storage,
            i_read_addr                  = self.read_addr_reg.storage,
            o_output_px_data             = self.OUTPUT_PX_DATA,
            i_rq_read                    = self.rq_read_reg.storage,
            i_reading                    = self.reading_reg.storage,
            o_ack_read                   = self.ACK_READ,
        )
        self.comb +=[
            self.zero_signal.eq(0),
            self.output_px_data_reg.status.eq(Cat(self.OUTPUT_PX_DATA[0:5],self.zero_signal,self.OUTPUT_PX_DATA[5:16])),
            self.ack_read_reg.status.eq(self.ACK_READ),
        ]
        