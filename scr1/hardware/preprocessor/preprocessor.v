module preprocessor#(parameter  depth = 76800, parameter addrBits = $clog2(depth))(
        input wire        prep_clk,
        input wire        reset,

        //Read buffer
        input wire        ack_read,
        output wire       rq_read,
        output wire       reading,
        input wire        [14:0] input_px_data,
        output wire       [addrBits-1:0] read_addr,
        output wire       read_clk,

        //Write buffer
        input wire        ack_write,
        output wire       rq_write,
        output wire       writing,
        output wire       [14:0] output_px_data,
        output wire       [addrBits-1:0] write_addr,
        output wire       write_clk,
        output reg        enable_mem,  

        //Preprocessor mode reg
        input wire        enable_mode_reg,
        input wire        [1:0] filter_reg,

        //Preprocessing SW flow ctrl reg
        input wire        prep_completed_SW_reg,
        output wire       prep_allowed_reg,

        //Preprocessing SW data reg
        input wire        [14:0] output_px_data_SW_reg,
        input wire        [addrBits-1:0] write_addr_SW_reg,
        output wire       [14:0] input_px_data_SW_reg,             
        input wire        [addrBits-1:0] read_addr_SW_reg,

        //IRQ
        output reg        start_prep_reg,
        output reg        finish_prep_reg
    );

    localparam MANAGER_IDLE = 0;
    localparam REQUESTING = 1;
    localparam PREPROCESSING = 2; 

    localparam PREP_IDLE = 0;
    localparam BYPASS = 1;
    localparam GRAYSCALE = 2;
    localparam THRESHOLD = 3;
    localparam INVERT = 4;
    localparam WAITING = 5;

    localparam THRESHOLD_VAL = 40;

    reg [1:0] manager_state;
    reg [2:0] prep_state;
    reg reader_enable;
    reg writer_enable;
    reg [addrBits-1:0] count_addr;
    reg [14:0] data;
    wire reader_allowed;
    wire writer_allowed;
    wire prep_completed;

    reg [14:0] output_px_data_HW;
    reg prep_allowed;
    reg prep_completed_HW;

    reg [7:0] red;
    reg [7:0] green;
    reg [7:0] blue;
    reg [7:0] gray_scale_1;
    reg [7:0] gray_scale_2;
    reg [4:0] gray_scale;
    reg [7:0] gray_scale_th;
    reg [7:0] gray_scale_inv;
    reg [7:0] r_inv;
    reg [7:0] g_inv;
    reg [7:0] b_inv;
    reg [7:0] value2;
    reg [7:0] value4;
    reg [7:0] value;
    reg [4:0] invert;
    reg [4:0] sum_rgb_inv;

    initial begin
        reader_enable = 1'b0;
        writer_enable = 1'b0;
        manager_state = MANAGER_IDLE;
        prep_state = PREP_IDLE;
        count_addr = 1'b0;
        enable_mem = 1'b0;
        output_px_data_HW = 15'b0;
        prep_allowed = 1'b0;
        prep_completed_HW = 1'b0;
        red = 8'b0;
        green = 8'b0;
        blue = 8'b0;
        gray_scale_1 = 8'b0;
        gray_scale_2 = 8'b0;
        gray_scale = 5'b0;
        gray_scale_th = 8'b0;
        gray_scale_inv = 8'b0;
        r_inv = 8'b0;
        g_inv = 8'b0;
        b_inv = 8'b0;
        invert = 5'b0;
        sum_rgb_inv = 5'b0;
        start_prep_reg = 1'b0;
        finish_prep_reg = 1'b0;
    end

    buffer_reader reader (
        .clk(prep_clk),
        .reset(reset),
        .enable(reader_enable),
        .read_allowed(reader_allowed),
        .ack_read(ack_read),
        .rq_read(rq_read),
        .reading(reading)
    );

    buffer_writer writer (
        .clk(prep_clk),
        .reset(reset),
        .enable(writer_enable),
        .write_allowed(writer_allowed),
        .ack_write(ack_write),
        .rq_write(rq_write),
        .writing(writing)
    );

    always @(posedge prep_clk)begin
        if(reset)begin
            manager_state <= MANAGER_IDLE;
            reader_enable <= 1'b0;
            writer_enable <= 1'b0;
            enable_mem <= 1'b0;
            prep_allowed <= 1'b0;
        end else begin
            case (manager_state)
                MANAGER_IDLE: begin
                    manager_state <= (~reader_allowed && ~writer_allowed)? REQUESTING : MANAGER_IDLE;
                end
                REQUESTING:begin
                    reader_enable <= 1'b1;
                    writer_enable <= 1'b1;
                    manager_state <= (reader_allowed && writer_allowed)? PREPROCESSING : REQUESTING;
                    prep_allowed <= (reader_allowed && writer_allowed)? 1'b1 : 1'b0 ;
                    enable_mem <= (reader_allowed && writer_allowed)? 1'b1 : 1'b0 ;
                end
                PREPROCESSING:begin
                    if(prep_completed)begin
                        reader_enable <= 1'b0;
                        writer_enable <= 1'b0;
                        manager_state <= MANAGER_IDLE;
                        prep_allowed <= 1'b0;
                        enable_mem <= 1'b0;
                    end 
                end
                default:begin
                    manager_state <= MANAGER_IDLE;
                end
            endcase
        end
    end 

    always@(posedge prep_clk)begin
        if(reset)begin
            prep_state <= PREP_IDLE;
            count_addr <= 17'b0;
            prep_completed_HW <= 1'b0;
            output_px_data_HW <= 15'b0;
            red <= 8'b0;
            green <= 8'b0;
            blue <= 8'b0;
            gray_scale_1 <= 8'b0;
            gray_scale_2 <= 8'b0;
            gray_scale <= 5'b0;
            gray_scale_th <= 8'b0;
            gray_scale_inv <= 8'b0;
            r_inv <= 8'b0;
            g_inv <= 8'b0;
            b_inv <= 8'b0;
            invert <= 5'b0;
            sum_rgb_inv <= 5'b0;
            start_prep_reg <= 1'b0;
            finish_prep_reg <= 1'b0;
        end else begin
            case(prep_state)
                PREP_IDLE:begin
                    if(enable_mode_reg == 1'b0 && prep_allowed)begin
                        case(filter_reg)
                            2'b00 : prep_state <= BYPASS;
                            2'b01 : prep_state <= GRAYSCALE;
                            2'b10 : prep_state <= THRESHOLD;
                            2'b11 : prep_state <= INVERT;
                            default : prep_state <= BYPASS;
                        endcase
                        start_prep_reg <= 1'b1;
                    end
                    finish_prep_reg <= 1'b0;
                end
                BYPASS:begin
                    if(count_addr < depth)begin
                        count_addr <= count_addr+1;
                        output_px_data_HW <= input_px_data;
                    end else begin
                        count_addr <= 17'b0;
                        prep_completed_HW <= 1'b1;
                        prep_state <= WAITING;
                    end
                end
                GRAYSCALE:begin
                    if(count_addr < depth)begin
                        count_addr <= count_addr+1;
                        //Stage 1
                        red <= input_px_data[14:10]<<3;
                        green <= input_px_data[9:5]<<3;
                        blue <= input_px_data[4:0]<<3;
                        //Stage 2
                        gray_scale_1 <= (red>>2)+(red>>5)+(green>>1);
                        gray_scale_2 <= (green>>4)+(blue>>4)+(blue>>5);
                        //Stage 3
                        gray_scale <= (gray_scale_1+gray_scale_2)>>3;
                        //Stage 4
                        output_px_data_HW <= {3{gray_scale}};
                    end else begin
                        count_addr <= 17'b0;
                        prep_completed_HW <= 1'b1;
                        prep_state <= WAITING;
                    end
                end
                THRESHOLD:begin
                    if(count_addr < depth)begin
                        count_addr <= count_addr+1;
                        //Stage 1
                        red <= input_px_data[14:10]<<3;
                        green <= input_px_data[9:5]<<3;
                        blue <= input_px_data[4:0]<<3;
                        //Stage 2
                        gray_scale_1 <= (red>>2)+(red>>5)+(green>>1);
                        gray_scale_2 <= (green>>4)+(blue>>4)+(blue>>5);
                        //Stage 3
                        gray_scale_th <= (gray_scale_1+gray_scale_2);
                        //Stage 4                    
                        output_px_data_HW <= (gray_scale_th < THRESHOLD_VAL)? 0 : 32767;
                    end else begin
                        count_addr <= 17'b0;
                        prep_completed_HW <= 1'b1;
                        prep_state <= WAITING;
                    end                    
                end
                INVERT:begin
                    if(count_addr < depth)begin
                        count_addr <= count_addr+1;
                        //Stage 1
                        red <= input_px_data[14:10]<<3;
                        green <= input_px_data[9:5]<<3;
                        blue <= input_px_data[4:0]<<3;
                        //Stage 2
                        // gray_scale_1 <= (red>>2)+(red>>5)+(green>>1);
                        // gray_scale_2 <= (green>>4)+(blue>>4)+(blue>>5);
                        //Stage 3
                        //gray_scale_inv <= 255 - (gray_scale_1+gray_scale_2);
                        r_inv <= 'd255 - red;
                        g_inv <= 'd255 - green;
                        b_inv <= 'd255 - blue;
                        //Stage 3
                        // invert <= gray_scale_inv>>3;
                        sum_rgb_inv <= ((r_inv) + (g_inv) + (b_inv))>>3;
                        //Stage 4
                        output_px_data_HW <= {3{sum_rgb_inv}};
                    end else begin
                        count_addr <= 17'b0;
                        prep_completed_HW <= 1'b1;
                        prep_state <= WAITING;
                    end
                end
                WAITING:begin
                    if(prep_allowed == 1'b0)begin
                        prep_completed_HW <= 1'b0;
                        prep_state <= PREP_IDLE;
                        output_px_data_HW <= 15'b0;
                        red <= 8'b0;
                        green <= 8'b0;
                        blue <= 8'b0;
                        gray_scale_1 <= 8'b0;
                        gray_scale_2 <= 8'b0;
                        gray_scale <= 5'b0;
                        gray_scale_th <= 8'b0;
                        gray_scale_inv <= 8'b0;
                        r_inv <= 8'b0;
                        g_inv <= 8'b0;
                        b_inv <= 8'b0;
                        invert <= 5'b0;
                        sum_rgb_inv <= 5'b0;
                        start_prep_reg <= 1'b0;
                        finish_prep_reg <= 1'b1;
                    end
                end
                default:begin
                    prep_state <= PREP_IDLE;
                end
            endcase
        end
    end

    assign read_clk = prep_clk;
    assign write_clk = prep_clk;
    assign prep_allowed_reg = prep_allowed;
    assign read_addr = (enable_mode_reg)? read_addr_SW_reg : count_addr + 2;
    assign write_addr = (enable_mode_reg)? write_addr_SW_reg : ((filter_reg == 2'b0)? count_addr : count_addr - 2);
    assign prep_completed = (enable_mode_reg)? prep_completed_SW_reg : prep_completed_HW;
    assign input_px_data_SW_reg = input_px_data;
    assign output_px_data = (enable_mode_reg)? output_px_data_SW_reg : output_px_data_HW;

endmodule
