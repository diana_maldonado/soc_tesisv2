from migen import *
from litex.soc.interconnect.csr import *
from litex.soc.interconnect.csr_eventmanager import *

import math

# Main module
class preprocessor(Module,AutoCSR):
    def __init__(self):

        ## Interrupciones
            self.submodules.ev                  = EventManager()
            self.ev.start_prep                  = EventSourceProcess(edge ="rising") 
            self.ev.finish_prep                 = EventSourceProcess(edge ="rising")

        ## Parameters
            depth = 76800
            addrBits = math.ceil(math.log(depth,2))

        ## Inputs        
            self.PREP_CLK                       = Signal()         
            self.PREP_RESET                     = Signal()
            self.PREP_ACK_READ                  = Signal()         #Read buffer
            self.PREP_INPUT_PX_DATA             = Signal(15)       #Read buffer
            self.PREP_ACK_WRITE                 = Signal()         #Write buffer
            
        ## Outputs
            self.PREP_RQ_READ                   = Signal()         #Read buffer
            self.PREP_READING                   = Signal()         #Read buffer
            self.PREP_READ_ADDR                 = Signal(addrBits) #Read buffer
            self.PREP_READ_CLK                  = Signal()         #Read buffer

            self.PREP_RQ_WRITE                  = Signal()
            self.PREP_WRITING                   = Signal()
            self.PREP_OUTPUT_PX_DATA            = Signal(15)
            self.PREP_WRITE_ADDR                = Signal(addrBits)
            self.PREP_WRITE_CLK                 = Signal()
            self.PREP_ENABLE_MEM                = Signal()

        ## Internal registers
            self.prep_enable_mode_reg           = CSRStorage()
            self.prep_filter_reg                = CSRStorage(2)
            self.prep_completed_SW_reg          = CSRStorage()
            self.prep_allowed_reg_CSR           = CSRStatus()
            self.prep_allowed_reg               = Signal()  
            self.prep_output_px_data_SW_reg     = CSRStorage(15)
            self.prep_write_addr_SW_reg         = CSRStorage(addrBits)
            self.prep_input_px_data_SW_reg_CSR  = CSRStatus(15)
            self.prep_input_px_data_SW_reg      = Signal(15) 
            self.prep_read_addr_SW_reg          = CSRStorage(addrBits)
            self.prep_start_wire                = Signal()
            self.prep_finish_wire               = Signal()

        ## Instances
            self.specials +=Instance("preprocessor",
                p_depth                         = depth,
                i_prep_clk                      = self.PREP_CLK,
                i_reset                         = self.PREP_RESET,
                i_ack_read                      = self.PREP_ACK_READ,
                o_rq_read                       = self.PREP_RQ_READ,
                o_reading                       = self.PREP_READING,
                i_input_px_data                 = self.PREP_INPUT_PX_DATA,
                o_read_addr                     = self.PREP_READ_ADDR,
                o_read_clk                      = self.PREP_READ_CLK,
                i_ack_write                     = self.PREP_ACK_WRITE,
                o_rq_write                      = self.PREP_RQ_WRITE,
                o_writing                       = self.PREP_WRITING,
                o_output_px_data                = self.PREP_OUTPUT_PX_DATA, 
                o_write_addr                    = self.PREP_WRITE_ADDR,
                o_write_clk                     = self.PREP_WRITE_CLK,
                o_enable_mem                    = self.PREP_ENABLE_MEM,
                i_enable_mode_reg               = self.prep_enable_mode_reg.storage,
                i_filter_reg                    = self.prep_filter_reg.storage,
                i_prep_completed_SW_reg         = self.prep_completed_SW_reg.storage,
                i_output_px_data_SW_reg         = self.prep_output_px_data_SW_reg.storage,
                i_write_addr_SW_reg             = self.prep_write_addr_SW_reg.storage,
                i_read_addr_SW_reg              = self.prep_read_addr_SW_reg.storage,
                o_prep_allowed_reg              = self.prep_allowed_reg,
                o_input_px_data_SW_reg          = self.prep_input_px_data_SW_reg,
                o_start_prep_reg                = self.prep_start_wire,
                o_finish_prep_reg               = self.prep_finish_wire
            )
            self.comb +=[
                self.prep_allowed_reg_CSR.status.eq(self.prep_allowed_reg),
                self.prep_input_px_data_SW_reg_CSR.status.eq(self.prep_input_px_data_SW_reg),
                self.ev.start_prep.trigger.eq(self.prep_start_wire),
                self.ev.finish_prep.trigger.eq(self.prep_finish_wire)   
            ]  
