################################################################################
# IO constraints
################################################################################
# serial:0.tx
set_property LOC J15 [get_ports {serial_tx}]
set_property IOSTANDARD LVCMOS33 [get_ports {serial_tx}]

# serial:0.rx
set_property LOC H15 [get_ports {serial_rx}]
set_property IOSTANDARD LVCMOS33 [get_ports {serial_rx}]

# cpu_reset:0
set_property LOC Y16 [get_ports {cpu_reset}]
set_property IOSTANDARD LVCMOS33 [get_ports {cpu_reset}]

# clk125:0
set_property LOC K17 [get_ports {clk125}]
set_property IOSTANDARD LVCMOS33 [get_ports {clk125}]

# pclk:0
set_property LOC Y7 [get_ports {pclk}]
set_property IOSTANDARD LVCMOS33 [get_ports {pclk}]

# cam_sync:0.xclk
set_property LOC V8 [get_ports {cam_sync_xclk}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_sync_xclk}]

# cam_sync:0.vsync
set_property LOC W8 [get_ports {cam_sync_vsync}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_sync_vsync}]

# cam_sync:0.href
set_property LOC U7 [get_ports {cam_sync_href}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_sync_href}]

# cam_data:0
set_property LOC U12 [get_ports {cam_data0}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_data0}]

# cam_data:1
set_property LOC T10 [get_ports {cam_data1}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_data1}]

# cam_data:2
set_property LOC T12 [get_ports {cam_data2}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_data2}]

# cam_data:3
set_property LOC T11 [get_ports {cam_data3}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_data3}]

# cam_data:4
set_property LOC Y14 [get_ports {cam_data4}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_data4}]

# cam_data:5
set_property LOC W15 [get_ports {cam_data5}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_data5}]

# cam_data:6
set_property LOC W14 [get_ports {cam_data6}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_data6}]

# cam_data:7
set_property LOC V15 [get_ports {cam_data7}]
set_property IOSTANDARD LVCMOS33 [get_ports {cam_data7}]

# LCD_spi:0.cs_n
set_property LOC T14 [get_ports {LCD_spi_cs_n}]
set_property IOSTANDARD LVCMOS33 [get_ports {LCD_spi_cs_n}]

# LCD_spi:0.dc_rs
set_property LOC T15 [get_ports {LCD_spi_dc_rs}]
set_property IOSTANDARD LVCMOS33 [get_ports {LCD_spi_dc_rs}]

# LCD_spi:0.mosi
set_property LOC P14 [get_ports {LCD_spi_mosi}]
set_property IOSTANDARD LVCMOS33 [get_ports {LCD_spi_mosi}]

# LCD_spi:0.clk
set_property LOC R14 [get_ports {LCD_spi_clk}]
set_property IOSTANDARD LVCMOS33 [get_ports {LCD_spi_clk}]

# LCD_spi:0.miso
set_property LOC U14 [get_ports {LCD_spi_miso}]
set_property IOSTANDARD LVCMOS33 [get_ports {LCD_spi_miso}]

# i2c:0.scl
set_property LOC V6 [get_ports {i2c_scl}]
set_property IOSTANDARD LVCMOS33 [get_ports {i2c_scl}]

# i2c:0.sda
set_property LOC W6 [get_ports {i2c_sda}]
set_property IOSTANDARD LVCMOS33 [get_ports {i2c_sda}]

# user_sw:0
set_property LOC T16 [get_ports {user_sw0}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_sw0}]

# user_sw:1
set_property LOC P15 [get_ports {user_sw1}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_sw1}]

# user_sw:2
set_property LOC G15 [get_ports {user_sw2}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_sw2}]

# user_btn:0
set_property LOC K18 [get_ports {user_btn}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_btn}]

################################################################################
# Design constraints
################################################################################

################################################################################
# Clock constraints
################################################################################


create_clock -name pclk_clk -period 125.0 [get_nets pclk_clk]

create_clock -name sys_clk -period 8.0 [get_nets sys_clk]

create_clock -name app1_clk -period 62.0 [get_nets app1_clk]

create_clock -name app2_clk -period 40.0 [get_nets app2_clk]

create_clock -name app3_clk -period 125.0 [get_nets app3_clk]

################################################################################
# False path constraints
################################################################################


set_false_path -quiet -through [get_nets -hierarchical -filter {mr_ff == TRUE}]

set_false_path -quiet -to [get_pins -filter {REF_PIN_NAME == PRE} -of_objects [get_cells -hierarchical -filter {ars_ff1 == TRUE || ars_ff2 == TRUE}]]

set_max_delay 2 -quiet -from [get_pins -filter {REF_PIN_NAME == C} -of_objects [get_cells -hierarchical -filter {ars_ff1 == TRUE}]] -to [get_pins -filter {REF_PIN_NAME == D} -of_objects [get_cells -hierarchical -filter {ars_ff2 == TRUE}]]