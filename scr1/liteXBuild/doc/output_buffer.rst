OUTPUT_BUFFER
=============

Register Listing for OUTPUT_BUFFER
----------------------------------

+----------------------------------------------------------------------------+------------------------------------------------------+
| Register                                                                   | Address                                              |
+============================================================================+======================================================+
| :ref:`OUTPUT_BUFFER_READ_CLK_REG <OUTPUT_BUFFER_READ_CLK_REG>`             | :ref:`0xf0005800 <OUTPUT_BUFFER_READ_CLK_REG>`       |
+----------------------------------------------------------------------------+------------------------------------------------------+
| :ref:`OUTPUT_BUFFER_OUTPUT_PX_DATA_REG <OUTPUT_BUFFER_OUTPUT_PX_DATA_REG>` | :ref:`0xf0005804 <OUTPUT_BUFFER_OUTPUT_PX_DATA_REG>` |
+----------------------------------------------------------------------------+------------------------------------------------------+
| :ref:`OUTPUT_BUFFER_READ_ADDR_REG <OUTPUT_BUFFER_READ_ADDR_REG>`           | :ref:`0xf0005808 <OUTPUT_BUFFER_READ_ADDR_REG>`      |
+----------------------------------------------------------------------------+------------------------------------------------------+
| :ref:`OUTPUT_BUFFER_RQ_READ_REG <OUTPUT_BUFFER_RQ_READ_REG>`               | :ref:`0xf000580c <OUTPUT_BUFFER_RQ_READ_REG>`        |
+----------------------------------------------------------------------------+------------------------------------------------------+
| :ref:`OUTPUT_BUFFER_READING_REG <OUTPUT_BUFFER_READING_REG>`               | :ref:`0xf0005810 <OUTPUT_BUFFER_READING_REG>`        |
+----------------------------------------------------------------------------+------------------------------------------------------+
| :ref:`OUTPUT_BUFFER_ACK_READ_REG <OUTPUT_BUFFER_ACK_READ_REG>`             | :ref:`0xf0005814 <OUTPUT_BUFFER_ACK_READ_REG>`       |
+----------------------------------------------------------------------------+------------------------------------------------------+

OUTPUT_BUFFER_READ_CLK_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0005800 + 0x0 = 0xf0005800`


    .. wavedrom::
        :caption: OUTPUT_BUFFER_READ_CLK_REG

        {
            "reg": [
                {"name": "read_clk_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


OUTPUT_BUFFER_OUTPUT_PX_DATA_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0005800 + 0x4 = 0xf0005804`


    .. wavedrom::
        :caption: OUTPUT_BUFFER_OUTPUT_PX_DATA_REG

        {
            "reg": [
                {"name": "output_px_data_reg[14:0]", "bits": 15},
                {"bits": 17},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


OUTPUT_BUFFER_READ_ADDR_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0005800 + 0x8 = 0xf0005808`


    .. wavedrom::
        :caption: OUTPUT_BUFFER_READ_ADDR_REG

        {
            "reg": [
                {"name": "read_addr_reg[16:0]", "bits": 17},
                {"bits": 15},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


OUTPUT_BUFFER_RQ_READ_REG
^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0005800 + 0xc = 0xf000580c`


    .. wavedrom::
        :caption: OUTPUT_BUFFER_RQ_READ_REG

        {
            "reg": [
                {"name": "rq_read_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


OUTPUT_BUFFER_READING_REG
^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0005800 + 0x10 = 0xf0005810`


    .. wavedrom::
        :caption: OUTPUT_BUFFER_READING_REG

        {
            "reg": [
                {"name": "reading_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


OUTPUT_BUFFER_ACK_READ_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0005800 + 0x14 = 0xf0005814`


    .. wavedrom::
        :caption: OUTPUT_BUFFER_ACK_READ_REG

        {
            "reg": [
                {"name": "ack_read_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


