SWITCHES
========

Register Listing for SWITCHES
-----------------------------

+----------------------------------+---------------------------------+
| Register                         | Address                         |
+==================================+=================================+
| :ref:`SWITCHES_IN <SWITCHES_IN>` | :ref:`0xf0003000 <SWITCHES_IN>` |
+----------------------------------+---------------------------------+

SWITCHES_IN
^^^^^^^^^^^

`Address: 0xf0003000 + 0x0 = 0xf0003000`

    GPIO Input(s) Status.

    .. wavedrom::
        :caption: SWITCHES_IN

        {
            "reg": [
                {"name": "in[2:0]", "bits": 3},
                {"bits": 29},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


