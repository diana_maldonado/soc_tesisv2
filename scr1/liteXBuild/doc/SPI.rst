SPI
===

Register Listing for SPI
------------------------

+------------------------------------------------------------+----------------------------------------------+
| Register                                                   | Address                                      |
+============================================================+==============================================+
| :ref:`SPI_OUTPUTDATAREGISTER <SPI_OUTPUTDATAREGISTER>`     | :ref:`0xf0001800 <SPI_OUTPUTDATAREGISTER>`   |
+------------------------------------------------------------+----------------------------------------------+
| :ref:`SPI_ENABLESPIREGISTER <SPI_ENABLESPIREGISTER>`       | :ref:`0xf0001804 <SPI_ENABLESPIREGISTER>`    |
+------------------------------------------------------------+----------------------------------------------+
| :ref:`SPI_ENABLECSREGISTER <SPI_ENABLECSREGISTER>`         | :ref:`0xf0001808 <SPI_ENABLECSREGISTER>`     |
+------------------------------------------------------------+----------------------------------------------+
| :ref:`SPI_ENABLEDCREGISTER <SPI_ENABLEDCREGISTER>`         | :ref:`0xf000180c <SPI_ENABLEDCREGISTER>`     |
+------------------------------------------------------------+----------------------------------------------+
| :ref:`SPI_PRESCALER_REG <SPI_PRESCALER_REG>`               | :ref:`0xf0001810 <SPI_PRESCALER_REG>`        |
+------------------------------------------------------------+----------------------------------------------+
| :ref:`SPI_INPUTDATAREGISTERCSR <SPI_INPUTDATAREGISTERCSR>` | :ref:`0xf0001814 <SPI_INPUTDATAREGISTERCSR>` |
+------------------------------------------------------------+----------------------------------------------+
| :ref:`SPI_DATACLOCKREGISTERCSR <SPI_DATACLOCKREGISTERCSR>` | :ref:`0xf0001818 <SPI_DATACLOCKREGISTERCSR>` |
+------------------------------------------------------------+----------------------------------------------+
| :ref:`SPI_EV_STATUS <SPI_EV_STATUS>`                       | :ref:`0xf000181c <SPI_EV_STATUS>`            |
+------------------------------------------------------------+----------------------------------------------+
| :ref:`SPI_EV_PENDING <SPI_EV_PENDING>`                     | :ref:`0xf0001820 <SPI_EV_PENDING>`           |
+------------------------------------------------------------+----------------------------------------------+
| :ref:`SPI_EV_ENABLE <SPI_EV_ENABLE>`                       | :ref:`0xf0001824 <SPI_EV_ENABLE>`            |
+------------------------------------------------------------+----------------------------------------------+

SPI_OUTPUTDATAREGISTER
^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001800 + 0x0 = 0xf0001800`


    .. wavedrom::
        :caption: SPI_OUTPUTDATAREGISTER

        {
            "reg": [
                {"name": "outputdataregister[7:0]", "bits": 8},
                {"bits": 24},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


SPI_ENABLESPIREGISTER
^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001800 + 0x4 = 0xf0001804`


    .. wavedrom::
        :caption: SPI_ENABLESPIREGISTER

        {
            "reg": [
                {"name": "enablespiregister", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


SPI_ENABLECSREGISTER
^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001800 + 0x8 = 0xf0001808`


    .. wavedrom::
        :caption: SPI_ENABLECSREGISTER

        {
            "reg": [
                {"name": "enablecsregister", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


SPI_ENABLEDCREGISTER
^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001800 + 0xc = 0xf000180c`


    .. wavedrom::
        :caption: SPI_ENABLEDCREGISTER

        {
            "reg": [
                {"name": "enabledcregister", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


SPI_PRESCALER_REG
^^^^^^^^^^^^^^^^^

`Address: 0xf0001800 + 0x10 = 0xf0001810`


    .. wavedrom::
        :caption: SPI_PRESCALER_REG

        {
            "reg": [
                {"name": "prescaler_reg[15:0]", "bits": 16},
                {"bits": 16},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


SPI_INPUTDATAREGISTERCSR
^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001800 + 0x14 = 0xf0001814`


    .. wavedrom::
        :caption: SPI_INPUTDATAREGISTERCSR

        {
            "reg": [
                {"name": "inputdataregistercsr[7:0]", "bits": 8},
                {"bits": 24},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


SPI_DATACLOCKREGISTERCSR
^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001800 + 0x18 = 0xf0001818`


    .. wavedrom::
        :caption: SPI_DATACLOCKREGISTERCSR

        {
            "reg": [
                {"name": "dataclockregistercsr", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


SPI_EV_STATUS
^^^^^^^^^^^^^

`Address: 0xf0001800 + 0x1c = 0xf000181c`

    This register contains the current raw level of the DataClock event trigger.
    Writes to this register have no effect.

    .. wavedrom::
        :caption: SPI_EV_STATUS

        {
            "reg": [
                {"name": "DataClock",  "bits": 1},
                {"bits": 31}
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


+-------+-----------+----------------------------------+
| Field | Name      | Description                      |
+=======+===========+==================================+
| [0]   | DATACLOCK | Level of the ``DataClock`` event |
+-------+-----------+----------------------------------+

SPI_EV_PENDING
^^^^^^^^^^^^^^

`Address: 0xf0001800 + 0x20 = 0xf0001820`

    When a  DataClock event occurs, the corresponding bit will be set in this
    register.  To clear the Event, set the corresponding bit in this register.

    .. wavedrom::
        :caption: SPI_EV_PENDING

        {
            "reg": [
                {"name": "DataClock",  "bits": 1},
                {"bits": 31}
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


+-------+-----------+-------------------------------------------------------------------------------+
| Field | Name      | Description                                                                   |
+=======+===========+===============================================================================+
| [0]   | DATACLOCK | `1` if a `DataClock` event occurred. This Event is triggered on a **falling** |
|       |           | edge.                                                                         |
+-------+-----------+-------------------------------------------------------------------------------+

SPI_EV_ENABLE
^^^^^^^^^^^^^

`Address: 0xf0001800 + 0x24 = 0xf0001824`

    This register enables the corresponding DataClock events.  Write a ``0`` to this
    register to disable individual events.

    .. wavedrom::
        :caption: SPI_EV_ENABLE

        {
            "reg": [
                {"name": "DataClock",  "bits": 1},
                {"bits": 31}
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


+-------+-----------+-------------------------------------------------+
| Field | Name      | Description                                     |
+=======+===========+=================================================+
| [0]   | DATACLOCK | Write a ``1`` to enable the ``DataClock`` Event |
+-------+-----------+-------------------------------------------------+

