PREPROCESSOR
============

Register Listing for PREPROCESSOR
---------------------------------

+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| Register                                                                                       | Address                                                        |
+================================================================================================+================================================================+
| :ref:`PREPROCESSOR_PREP_ENABLE_MODE_REG <PREPROCESSOR_PREP_ENABLE_MODE_REG>`                   | :ref:`0xf0000800 <PREPROCESSOR_PREP_ENABLE_MODE_REG>`          |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_PREP_FILTER_REG <PREPROCESSOR_PREP_FILTER_REG>`                             | :ref:`0xf0000804 <PREPROCESSOR_PREP_FILTER_REG>`               |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_PREP_COMPLETED_SW_REG <PREPROCESSOR_PREP_COMPLETED_SW_REG>`                 | :ref:`0xf0000808 <PREPROCESSOR_PREP_COMPLETED_SW_REG>`         |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_PREP_ALLOWED_REG_CSR <PREPROCESSOR_PREP_ALLOWED_REG_CSR>`                   | :ref:`0xf000080c <PREPROCESSOR_PREP_ALLOWED_REG_CSR>`          |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_PREP_OUTPUT_PX_DATA_SW_REG <PREPROCESSOR_PREP_OUTPUT_PX_DATA_SW_REG>`       | :ref:`0xf0000810 <PREPROCESSOR_PREP_OUTPUT_PX_DATA_SW_REG>`    |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_PREP_WRITE_ADDR_SW_REG <PREPROCESSOR_PREP_WRITE_ADDR_SW_REG>`               | :ref:`0xf0000814 <PREPROCESSOR_PREP_WRITE_ADDR_SW_REG>`        |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_PREP_INPUT_PX_DATA_SW_REG_CSR <PREPROCESSOR_PREP_INPUT_PX_DATA_SW_REG_CSR>` | :ref:`0xf0000818 <PREPROCESSOR_PREP_INPUT_PX_DATA_SW_REG_CSR>` |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_PREP_READ_ADDR_SW_REG <PREPROCESSOR_PREP_READ_ADDR_SW_REG>`                 | :ref:`0xf000081c <PREPROCESSOR_PREP_READ_ADDR_SW_REG>`         |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_EV_STATUS <PREPROCESSOR_EV_STATUS>`                                         | :ref:`0xf0000820 <PREPROCESSOR_EV_STATUS>`                     |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_EV_PENDING <PREPROCESSOR_EV_PENDING>`                                       | :ref:`0xf0000824 <PREPROCESSOR_EV_PENDING>`                    |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| :ref:`PREPROCESSOR_EV_ENABLE <PREPROCESSOR_EV_ENABLE>`                                         | :ref:`0xf0000828 <PREPROCESSOR_EV_ENABLE>`                     |
+------------------------------------------------------------------------------------------------+----------------------------------------------------------------+

PREPROCESSOR_PREP_ENABLE_MODE_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x0 = 0xf0000800`


    .. wavedrom::
        :caption: PREPROCESSOR_PREP_ENABLE_MODE_REG

        {
            "reg": [
                {"name": "prep_enable_mode_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


PREPROCESSOR_PREP_FILTER_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x4 = 0xf0000804`


    .. wavedrom::
        :caption: PREPROCESSOR_PREP_FILTER_REG

        {
            "reg": [
                {"name": "prep_filter_reg[1:0]", "bits": 2},
                {"bits": 30},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


PREPROCESSOR_PREP_COMPLETED_SW_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x8 = 0xf0000808`


    .. wavedrom::
        :caption: PREPROCESSOR_PREP_COMPLETED_SW_REG

        {
            "reg": [
                {"name": "prep_completed_sw_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


PREPROCESSOR_PREP_ALLOWED_REG_CSR
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0xc = 0xf000080c`


    .. wavedrom::
        :caption: PREPROCESSOR_PREP_ALLOWED_REG_CSR

        {
            "reg": [
                {"name": "prep_allowed_reg_csr", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


PREPROCESSOR_PREP_OUTPUT_PX_DATA_SW_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x10 = 0xf0000810`


    .. wavedrom::
        :caption: PREPROCESSOR_PREP_OUTPUT_PX_DATA_SW_REG

        {
            "reg": [
                {"name": "prep_output_px_data_sw_reg[14:0]", "bits": 15},
                {"bits": 17},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


PREPROCESSOR_PREP_WRITE_ADDR_SW_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x14 = 0xf0000814`


    .. wavedrom::
        :caption: PREPROCESSOR_PREP_WRITE_ADDR_SW_REG

        {
            "reg": [
                {"name": "prep_write_addr_sw_reg[16:0]", "bits": 17},
                {"bits": 15},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


PREPROCESSOR_PREP_INPUT_PX_DATA_SW_REG_CSR
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x18 = 0xf0000818`


    .. wavedrom::
        :caption: PREPROCESSOR_PREP_INPUT_PX_DATA_SW_REG_CSR

        {
            "reg": [
                {"name": "prep_input_px_data_sw_reg_csr[14:0]", "bits": 15},
                {"bits": 17},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


PREPROCESSOR_PREP_READ_ADDR_SW_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x1c = 0xf000081c`


    .. wavedrom::
        :caption: PREPROCESSOR_PREP_READ_ADDR_SW_REG

        {
            "reg": [
                {"name": "prep_read_addr_sw_reg[16:0]", "bits": 17},
                {"bits": 15},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


PREPROCESSOR_EV_STATUS
^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x20 = 0xf0000820`

    This register contains the current raw level of the finish_prep event trigger.
    Writes to this register have no effect.

    .. wavedrom::
        :caption: PREPROCESSOR_EV_STATUS

        {
            "reg": [
                {"name": "start_prep",  "bits": 1},
                {"name": "finish_prep",  "bits": 1},
                {"bits": 30}
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


+-------+-------------+------------------------------------+
| Field | Name        | Description                        |
+=======+=============+====================================+
| [0]   | START_PREP  | Level of the ``start_prep`` event  |
+-------+-------------+------------------------------------+
| [1]   | FINISH_PREP | Level of the ``finish_prep`` event |
+-------+-------------+------------------------------------+

PREPROCESSOR_EV_PENDING
^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x24 = 0xf0000824`

    When a  finish_prep event occurs, the corresponding bit will be set in this
    register.  To clear the Event, set the corresponding bit in this register.

    .. wavedrom::
        :caption: PREPROCESSOR_EV_PENDING

        {
            "reg": [
                {"name": "start_prep",  "bits": 1},
                {"name": "finish_prep",  "bits": 1},
                {"bits": 30}
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


+-------+-------------+---------------------------------------------------------------------------------+
| Field | Name        | Description                                                                     |
+=======+=============+=================================================================================+
| [0]   | START_PREP  | `1` if a `start_prep` event occurred. This Event is triggered on a **falling**  |
|       |             | edge.                                                                           |
+-------+-------------+---------------------------------------------------------------------------------+
| [1]   | FINISH_PREP | `1` if a `finish_prep` event occurred. This Event is triggered on a **falling** |
|       |             | edge.                                                                           |
+-------+-------------+---------------------------------------------------------------------------------+

PREPROCESSOR_EV_ENABLE
^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000800 + 0x28 = 0xf0000828`

    This register enables the corresponding finish_prep events.  Write a ``0`` to
    this register to disable individual events.

    .. wavedrom::
        :caption: PREPROCESSOR_EV_ENABLE

        {
            "reg": [
                {"name": "start_prep",  "bits": 1},
                {"name": "finish_prep",  "bits": 1},
                {"bits": 30}
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


+-------+-------------+---------------------------------------------------+
| Field | Name        | Description                                       |
+=======+=============+===================================================+
| [0]   | START_PREP  | Write a ``1`` to enable the ``start_prep`` Event  |
+-------+-------------+---------------------------------------------------+
| [1]   | FINISH_PREP | Write a ``1`` to enable the ``finish_prep`` Event |
+-------+-------------+---------------------------------------------------+

