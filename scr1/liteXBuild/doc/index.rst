
Documentation for LiteX SoC Project
===================================



Modules
=======

.. toctree::
    :maxdepth: 1

    interrupts

Register Groups
===============

.. toctree::
    :maxdepth: 1

    cam_read
    preprocessor
    prep_buffer
    SPI
    i2c
    switches
    button
    RTC
    ctrl
    identifier_mem
    output_buffer
    timer0
    uart

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

