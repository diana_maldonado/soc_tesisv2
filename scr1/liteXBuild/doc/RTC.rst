RTC
===

Register Listing for RTC
------------------------

+--------------------------------------------------+-----------------------------------------+
| Register                                         | Address                                 |
+==================================================+=========================================+
| :ref:`RTC_MILISEC_REG_CSR <RTC_MILISEC_REG_CSR>` | :ref:`0xf0004000 <RTC_MILISEC_REG_CSR>` |
+--------------------------------------------------+-----------------------------------------+

RTC_MILISEC_REG_CSR
^^^^^^^^^^^^^^^^^^^

`Address: 0xf0004000 + 0x0 = 0xf0004000`


    .. wavedrom::
        :caption: RTC_MILISEC_REG_CSR

        {
            "reg": [
                {"name": "milisec_reg_csr[31:0]", "bits": 32}
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


