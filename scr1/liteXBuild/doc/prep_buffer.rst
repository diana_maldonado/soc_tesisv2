PREP_BUFFER
===========

Register Listing for PREP_BUFFER
--------------------------------

+------------------------------------------------------------------------+----------------------------------------------------+
| Register                                                               | Address                                            |
+========================================================================+====================================================+
| :ref:`PREP_BUFFER_READ_CLK_REG <PREP_BUFFER_READ_CLK_REG>`             | :ref:`0xf0001000 <PREP_BUFFER_READ_CLK_REG>`       |
+------------------------------------------------------------------------+----------------------------------------------------+
| :ref:`PREP_BUFFER_OUTPUT_PX_DATA_REG <PREP_BUFFER_OUTPUT_PX_DATA_REG>` | :ref:`0xf0001004 <PREP_BUFFER_OUTPUT_PX_DATA_REG>` |
+------------------------------------------------------------------------+----------------------------------------------------+
| :ref:`PREP_BUFFER_READ_ADDR_REG <PREP_BUFFER_READ_ADDR_REG>`           | :ref:`0xf0001008 <PREP_BUFFER_READ_ADDR_REG>`      |
+------------------------------------------------------------------------+----------------------------------------------------+
| :ref:`PREP_BUFFER_RQ_READ_REG <PREP_BUFFER_RQ_READ_REG>`               | :ref:`0xf000100c <PREP_BUFFER_RQ_READ_REG>`        |
+------------------------------------------------------------------------+----------------------------------------------------+
| :ref:`PREP_BUFFER_READING_REG <PREP_BUFFER_READING_REG>`               | :ref:`0xf0001010 <PREP_BUFFER_READING_REG>`        |
+------------------------------------------------------------------------+----------------------------------------------------+
| :ref:`PREP_BUFFER_ACK_READ_REG <PREP_BUFFER_ACK_READ_REG>`             | :ref:`0xf0001014 <PREP_BUFFER_ACK_READ_REG>`       |
+------------------------------------------------------------------------+----------------------------------------------------+

PREP_BUFFER_READ_CLK_REG
^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001000 + 0x0 = 0xf0001000`


    .. wavedrom::
        :caption: PREP_BUFFER_READ_CLK_REG

        {
            "reg": [
                {"name": "read_clk_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


PREP_BUFFER_OUTPUT_PX_DATA_REG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001000 + 0x4 = 0xf0001004`


    .. wavedrom::
        :caption: PREP_BUFFER_OUTPUT_PX_DATA_REG

        {
            "reg": [
                {"name": "output_px_data_reg[14:0]", "bits": 15},
                {"bits": 17},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


PREP_BUFFER_READ_ADDR_REG
^^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001000 + 0x8 = 0xf0001008`


    .. wavedrom::
        :caption: PREP_BUFFER_READ_ADDR_REG

        {
            "reg": [
                {"name": "read_addr_reg[16:0]", "bits": 17},
                {"bits": 15},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 1 }, "options": {"hspace": 400, "bits": 32, "lanes": 1}
        }


PREP_BUFFER_RQ_READ_REG
^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001000 + 0xc = 0xf000100c`


    .. wavedrom::
        :caption: PREP_BUFFER_RQ_READ_REG

        {
            "reg": [
                {"name": "rq_read_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


PREP_BUFFER_READING_REG
^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001000 + 0x10 = 0xf0001010`


    .. wavedrom::
        :caption: PREP_BUFFER_READING_REG

        {
            "reg": [
                {"name": "reading_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


PREP_BUFFER_ACK_READ_REG
^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0001000 + 0x14 = 0xf0001014`


    .. wavedrom::
        :caption: PREP_BUFFER_ACK_READ_REG

        {
            "reg": [
                {"name": "ack_read_reg", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


