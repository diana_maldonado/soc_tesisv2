CAM_READ
========

Register Listing for CAM_READ
-----------------------------

+------------------------------------------------------------+----------------------------------------------+
| Register                                                   | Address                                      |
+============================================================+==============================================+
| :ref:`CAM_READ_CAM_ENABLE_XCLK <CAM_READ_CAM_ENABLE_XCLK>` | :ref:`0xf0000000 <CAM_READ_CAM_ENABLE_XCLK>` |
+------------------------------------------------------------+----------------------------------------------+

CAM_READ_CAM_ENABLE_XCLK
^^^^^^^^^^^^^^^^^^^^^^^^

`Address: 0xf0000000 + 0x0 = 0xf0000000`


    .. wavedrom::
        :caption: CAM_READ_CAM_ENABLE_XCLK

        {
            "reg": [
                {"name": "cam_enable_xclk", "bits": 1},
                {"bits": 31},
            ], "config": {"hspace": 400, "bits": 32, "lanes": 4 }, "options": {"hspace": 400, "bits": 32, "lanes": 4}
        }


