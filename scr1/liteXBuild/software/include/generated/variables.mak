PACKAGES=libcompiler_rt libbase libfatfs liblitespi liblitedram libliteeth liblitesdcard liblitesata bios
PACKAGE_DIRS=/opt/liteX/litex/litex/soc/software/libcompiler_rt /opt/liteX/litex/litex/soc/software/libbase /opt/liteX/litex/litex/soc/software/libfatfs /opt/liteX/litex/litex/soc/software/liblitespi /opt/liteX/litex/litex/soc/software/liblitedram /opt/liteX/litex/litex/soc/software/libliteeth /opt/liteX/litex/litex/soc/software/liblitesdcard /opt/liteX/litex/litex/soc/software/liblitesata /opt/liteX/litex/litex/soc/software/bios
LIBS=libcompiler_rt libbase-nofloat libfatfs liblitespi liblitedram libliteeth liblitesdcard liblitesata
TRIPLE=riscv64-unknown-elf
CPU=vexriscv
CPUFLAGS=-march=rv32im     -mabi=ilp32 -D__vexriscv__
CPUENDIANNESS=little
CLANG=0
CPU_DIRECTORY=/opt/liteX/litex/litex/soc/cores/cpu/vexriscv
SOC_DIRECTORY=/opt/liteX/litex/litex/soc
COMPILER_RT_DIRECTORY=/opt/liteX/pythondata-software-compiler_rt/pythondata_software_compiler_rt/data
export BUILDINC_DIRECTORY
BUILDINC_DIRECTORY=/home/jhon/Documents/Materias/soc_tesisv2/liteXBuild/software/include
LIBCOMPILER_RT_DIRECTORY=/opt/liteX/litex/litex/soc/software/libcompiler_rt
LIBBASE_DIRECTORY=/opt/liteX/litex/litex/soc/software/libbase
LIBFATFS_DIRECTORY=/opt/liteX/litex/litex/soc/software/libfatfs
LIBLITESPI_DIRECTORY=/opt/liteX/litex/litex/soc/software/liblitespi
LIBLITEDRAM_DIRECTORY=/opt/liteX/litex/litex/soc/software/liblitedram
LIBLITEETH_DIRECTORY=/opt/liteX/litex/litex/soc/software/libliteeth
LIBLITESDCARD_DIRECTORY=/opt/liteX/litex/litex/soc/software/liblitesdcard
LIBLITESATA_DIRECTORY=/opt/liteX/litex/litex/soc/software/liblitesata
BIOS_DIRECTORY=/opt/liteX/litex/litex/soc/software/bios